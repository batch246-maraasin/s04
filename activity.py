from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass

    @abstractclassmethod
    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_breed(self):
        return self.breed

    def set_breed(self, breed):
        self.breed = breed

    def get_age(self):
        return self.age

    def set_age(self, age):
        self.age = age

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self.name} !")

class Cat(Animal):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_breed(self):
        return self.breed

    def set_breed(self, breed):
        self.breed = breed

    def get_age(self):
        return self.age

    def set_age(self, age):
        self.age = age

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaa!")

    def call(self):
        print(f"{self.name} come on!")


# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()


